# ---------------------------------------------------- #
# File: hw2.py
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# Stephen Garza
# Monica Parucho 
# BitBucket handle: sgarza1
# BitBucket link: https://bitbucket.org/sgarza1/hw2-9.30.2014/commits/fda7aaab1dc33da1151fab3f5f07037740e60c4e
# ---------------------------------------------------- #
# Plaftorm:    Stephen-Unix Monica-Windows
# Environment: Python 2.7.7
# Libaries:    numpy
#              matplotlib
#       	   scipy 
#			   glob
#			   skimage
#			   All external libraries from Anaconda 2.0.1 (x86_64)
#              
#       	 
# ---------------------------------------------------- #
# Description: Fulfills hw2 requirements
# ---------------------------------------------------- #



import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
import time
from skimage.feature import corner_harris, corner_subpix, corner_peaks
'''corner code from
http://scikit-image.org/docs/dev/auto_examples/plot_corner.html'''
class Triangle:

	def __init__(self, height=0, width=0): # The parameters height, width, area aren't called in main()
		self.height = height
		self.width = width
		self.area = self.getArea()

	def getArea(self): # This function is not called in main()
		return self.height * self.width / 2

	def draw_triangle(self,sigma,bound=64): #draws a triangle
		
		win=256
		sq_im = np.zeros((win,win))
		sq_im[bound:win-bound,bound:win-bound]=1
		tri = np.tril(sq_im, -1)
		plt.title('Original Triangle')
		plt.axis('off')

		blurry_im = ndimage.gaussian_filter(tri,sigma)

		self.clear_pic=tri
		self.blur_pic=blurry_im

		plt.imshow(blurry_im, cmap=plt.cm.gray)
		plt.show()



	def save_rotated_images(self,im,deg,step,iterations): #spins triangle. Saves images of rotated triangles
		
		for counter in range(iterations):

			self.rotate_im=ndimage.rotate(im,(step*counter), reshape=False)
			misc.imsave('pics%02d.png' % counter, self.rotate_im)
			

	def get_edges(self,im): #finds the edges inside an image. 
		sx = ndimage.sobel(misc.imread(im),axis=1)
		sy = ndimage.sobel(misc.imread(im),axis=0)
		sob = np.hypot(sx, sy)
		plt.imshow(sy, cmap=plt.cm.gray)
		plt.title('Edges from a rotated image')
		plt.axis('off')
		plt.show()


	def save_corner_images(self,iterations): #uses harris filter. Saves images with vertices overlayed on triangle
		#harris filter preformed well for sigma = 0 and sigma = 2
		

		for counter in range(iterations):
			image=('pics%02d.png'%counter)

			coords = corner_peaks(corner_harris(misc.imread(image)), min_distance=5)
			
		
			plt.imshow(misc.imread(image), interpolation='nearest', cmap=plt.cm.gray)
			
			im=plt.plot(coords[0, 1], coords[0, 0], '.y', markersize=3)
			im=plt.plot(coords[1, 1], coords[1, 0], '.g', markersize=3)
			im=plt.plot(coords[2, 1], coords[2, 0], '.r', markersize=3)
			
			
			
			plt.axis('off')
			plt.savefig('corner%02d.png'%counter)
			plt.hold('off')


	def get_binary_image(self,im,sigma=0): #displays a single binary image

		#doesn't work well when sigma=2. Works better with sigma=0
		

		binary_im = ndimage.binary_opening(im)
		binary_im = ndimage.gaussian_filter(binary_im,sigma)
		plt.imshow((im-binary_im),cmap=plt.cm.gray)
		plt.title('Vertices from Original Triangle')
		plt.axis('off')
		plt.show()

	def update_animation(self,ii): #reads in pics w/o corners marked
		im=plt.imshow(misc.imread('pics%02d.png' % ii),cmap=plt.cm.gray)
		return im
		

	def update_corner_animation(self,ii):#reads in pics w/ corners marked

		im=plt.imshow(misc.imread('corner%02d.png' % ii),cmap=plt.cm.gray)
		plt.axis('off')
		plt.title('Flow Visulization of Vertices')
		return im
		


	def animate(self,iterations,interval=10): #images images w/o marked corners
		fig=plt.figure()
		ani = animation.FuncAnimation(fig,tri.update_animation, frames=range(iterations), interval=interval, repeat=True)
		plt.axis('off')
		plt.title('Rotating Triangle')
		plt.show()
		#ani.save('ani_rotate.mp4') #this would save your animation to your cd. I do not have the software to save mp4's from sublim

	def animate_vertices_harris(self,iterations,interval=10): #using this as for optical flow.
		fig=plt.figure()
		ani = animation.FuncAnimation(fig,tri.update_corner_animation, frames=range(iterations), interval=interval, repeat=True)
		plt.show()

		#ani.save('ani_rotate_vertices.mp4') #this would save your animation to your cd. I do not have the software to save mp4's from sublim


		#this could be sped up by decreasing the interval size
		#this could be sped up by imputing images directly instead of saving them and loading them back in
		#this is done using the harris method to track the vertices in each image.
		#once tracked, I simply plotted the string of images to show their paths
def main():

	deg=360 #specifies final degree of rotation
	step=10 #step size in degrees. Determines how many images to take
	iterations=deg/step+1 #number of images. Include an extra for 0 deg rotation
	sigma=int(raw_input("Enter 0 or 2 for Gaussian filter parameter."))
	print ''
	size=64 #size of triangle
	interval=10 #will be angular velocity of animation. Inversely proportional to rotational speed

	opition=raw_input("Enter 'f' for fast angular velocity. Enter 's' for slow angular velocity: ")
	if opition=='s':
		interval=1000
	if opition=='f':
		interval=10

	global tri
	tri=Triangle(4,5) #initalizes triangle obect. The height and width do not influde the image or problem at all
	
	tri.draw_triangle(sigma,size) 
	tri.save_rotated_images(tri.blur_pic,deg,step,iterations)
	tri.animate(iterations,interval)
	
	tri.get_edges('pics01.png')
	tri.get_binary_image(tri.clear_pic) #finds vertices from a binary arrary
	

	print 'Loading...'	
	tri.save_corner_images(iterations)#takes rotated images and overlays vertices on them.
	tri.animate_vertices_harris(iterations,interval)

main()
